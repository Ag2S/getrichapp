package org.alan.getrich.main;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.alan.getrich.R;
import org.alan.getrich.asset.AssetAndPrice;
import org.alan.getrich.asset.InsertNewAssetActivity;

import java.text.DecimalFormat;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class SummaryPortfolioFragment extends Fragment {
    private MainActivityViewModel viewModel;
    private RecyclerView recyclerView;
    private ActivityResultLauncher<AssetSummary.Input> assetSummaryLauncher;
    private PieChart pieChart;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_summary_portfolio, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(this).get(MainActivityViewModel.class);
        Button newAssetButton = view.findViewById(R.id.new_asset_button_for_portfolio);
        newAssetButton.setOnClickListener(v -> {
            Intent intent = new Intent(requireActivity(), InsertNewAssetActivity.class);
            startActivity(intent);
        });
        pieChart = requireActivity().findViewById(R.id.chart_asset_summary);
        recyclerView = view.findViewById(R.id.asset_list);

        assetSummaryLauncher = registerForActivityResult(new AssetSummary(), unused -> {
        });

        viewModel.getTotalAssetAndPrices().observe(getViewLifecycleOwner(),
                assetAndPrices -> {
                    recyclerView.setAdapter(
                            new AssetListRecyclerViewAdapter(
                                    requireActivity(), assetAndPrices, asset -> assetSummaryLauncher.launch(AssetSummary.view(asset))));
                    setPieChart(assetAndPrices);
                });

        TextView xirrTextview = view.findViewById(R.id.xirr);
        viewModel.refreshXirr().observe(getViewLifecycleOwner(), xirr -> {
            DecimalFormat decimalFormat = new DecimalFormat("##.##");
            String xirrString = getString(R.string.xirr) + decimalFormat.format(xirr * 100) + "%";
            xirrTextview.setText(xirrString);
        });
        TextView cagrTextview = view.findViewById(R.id.cagr);
        viewModel.refreshCagr().observe(getViewLifecycleOwner(), cagr -> {
            DecimalFormat decimalFormat = new DecimalFormat("##.##");
            String cagrString = getString(R.string.cagr) + decimalFormat.format(cagr * 100) + "%";
            cagrTextview.setText(cagrString);
        });
        TextView rateOfReturnTextview = view.findViewById(R.id.rate_of_return);
        viewModel.refreshRateOfReturn().observe(getViewLifecycleOwner(), rateOfReturn -> {
            DecimalFormat decimalFormat = new DecimalFormat("##.##");
            String rateOfReturnString = getString(R.string.rateOfReturn) + decimalFormat.format(rateOfReturn * 100) + "%";
            rateOfReturnTextview.setText(rateOfReturnString);
        });
    }

    private void setPieChart(List<AssetAndPrice> assetAndPrices) {
        pieChart.setData(getPieData(assetAndPrices, getString(R.string.blank)));
        pieChart.setNoDataText(getString(R.string.no_chart_data));
        pieChart.getDescription().setText(getString(R.string.pie_chart_label));
        pieChart.animateY(1400, Easing.EaseInOutQuad);
        pieChart.invalidate();
        pieChart.setEntryLabelColor(Color.BLACK);
        pieChart.setEntryLabelTextSize(15f);
    }

    private PieData getPieData(List<AssetAndPrice> assetAndPrices, String label) {
        List<PieEntry> pieEntries = assetAndPrices.stream()
                .map(assetAndPrice -> new PieEntry(
                        (float) (assetAndPrice.price * assetAndPrice.asset.amounts),
                        assetAndPrice.asset.assetInfo.name))
                .collect(Collectors.toList());
        PieDataSet pieDataSet = new PieDataSet(pieEntries, label);
        ArrayList<Integer> colors = new ArrayList<>();
        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);
        pieDataSet.setColors(colors);
        pieDataSet.setValueTextSize(12f);
        pieDataSet.setValueTextColor(Color.GRAY);
        return new PieData(pieDataSet);
    }
}