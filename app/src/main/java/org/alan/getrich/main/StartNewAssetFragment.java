package org.alan.getrich.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import org.alan.getrich.R;
import org.alan.getrich.asset.InsertNewAssetActivity;

public class StartNewAssetFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_start_new_asset, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Button button = requireActivity().findViewById(R.id.new_asset_button);
        button.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), InsertNewAssetActivity.class);
            startActivity(intent);
        });
    }
}
