package org.alan.getrich.main;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import org.alan.getrich.R;
import org.alan.getrich.asset.Asset;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private MainActivityViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setFragment(new WaitingFragment());
        viewModel = new ViewModelProvider(this).get(MainActivityViewModel.class);
        viewModel.refreshAssets().observe(this, this::setFragmentByAssets);
    }

    private void setFragmentByAssets(List<Asset> assets) {
        if (assets.isEmpty()) {
            setFragment(new StartNewAssetFragment());
        } else {
            setFragment(new SummaryPortfolioFragment());
        }
    }

    private void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_activity_fragment_container, fragment);
        fragmentTransaction.commit();
    }
}