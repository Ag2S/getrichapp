package org.alan.getrich.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.alan.getrich.asset.Asset;
import org.alan.getrich.R;
import org.alan.getrich.asset.AssetAndPrice;

import java.util.List;
import java.util.function.Consumer;

public class AssetListRecyclerViewAdapter extends RecyclerView.Adapter<AssetListRecyclerViewAdapter.ViewHolder> {
    private final Context context;
    private final List<AssetAndPrice> assetAndPrices;
    private final Consumer<Asset> onAssetClickedListener;

    AssetListRecyclerViewAdapter(@NonNull Context context, List<AssetAndPrice> assetAndPrices, Consumer<Asset> onAssetClickedListener) {
        this.context = context;
        this.assetAndPrices = assetAndPrices;
        this.onAssetClickedListener = onAssetClickedListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.assets_layout_for_summary, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AssetListRecyclerViewAdapter.ViewHolder holder, int position) {
        AssetAndPrice currentAssetAndPrice = assetAndPrices.get(position);

        String setTextString = context.getString(currentAssetAndPrice.asset.assetInfo.type.getStringRes()) + "," +
                currentAssetAndPrice.asset.assetInfo.currency.getDisplayName() + "," +
                currentAssetAndPrice.asset.assetInfo.name + "," +
                currentAssetAndPrice.asset.amounts * currentAssetAndPrice.price;
        holder.assetKey.setText(setTextString);
        holder.assetKey.setOnClickListener(view -> onAssetClickedListener.accept(currentAssetAndPrice.asset));
    }

    @Override
    public int getItemCount() {
        return assetAndPrices.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        Button assetKey;

        ViewHolder(View itemView) {
            super(itemView);
            assetKey = itemView.findViewById(R.id.asset_for_summary);
        }
    }
}
