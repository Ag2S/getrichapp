package org.alan.getrich.main;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import org.alan.getrich.ReturnUtil;
import org.alan.getrich.asset.Asset;
import org.alan.getrich.asset.AssetAndPrice;
import org.alan.getrich.asset.AssetRecord;
import org.alan.getrich.asset.AssetService;

import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class MainActivityViewModel extends ViewModel {
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();


    public LiveData<List<Asset>> refreshAssets() {
        MutableLiveData<List<Asset>> assetsLiveData = new MutableLiveData<>();
        Disposable disposable = AssetService.getInstance().getAssetsAsync()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(assetsLiveData::setValue);
        compositeDisposable.add(disposable);
        return assetsLiveData;
    }

    public LiveData<Double> refreshXirr() {
        MutableLiveData<Double> xirr = new MutableLiveData<>();
        Disposable disposable = getTotalAssetRecords()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(totalAssetRecords -> xirr.setValue(ReturnUtil.getXirr(totalAssetRecords)));
        compositeDisposable.add(disposable);
        return xirr;
    }

    public LiveData<Double> refreshCagr() {
        MutableLiveData<Double> cagr = new MutableLiveData<>();
        Disposable disposable = getTotalAssetRecords()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(totalAssetRecords -> cagr.setValue(ReturnUtil.getCagr(totalAssetRecords::stream)));
        compositeDisposable.add(disposable);
        return cagr;
    }


    public LiveData<Double> refreshRateOfReturn() {
        MutableLiveData<Double> rateOfReturn = new MutableLiveData<>();
        Disposable disposable = getTotalAssetRecords()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(totalAssetRecords -> rateOfReturn.setValue(ReturnUtil.getRateOfReturn(totalAssetRecords::stream)));
        compositeDisposable.add(disposable);
        return rateOfReturn;
    }

    @NonNull
    private static Single<List<AssetRecord>> getTotalAssetRecords() {
        return Single.fromCallable(() -> {
            List<Asset> assets = AssetService.getInstance().getAssetsAsync().blockingGet();
            return Stream.concat(
                    assets.stream()
                            .map(asset -> AssetService.getInstance().getAssetRecordsByIdAsync(asset.id)
                                    .blockingGet())
                            .flatMap(Collection::stream),
                    assets.stream()
                            .filter(asset -> asset.amounts != 0)
                            .map(asset -> AssetRecord.assetRecordWithoutID(
                                    asset.amounts,
                                    AssetService.getInstance().getAssetCurrentPriceAsync(asset)
                                            .blockingGet(),
                                    Calendar.getInstance(),
                                    AssetRecord.Type.SOLD
                            ))
            ).collect(Collectors.toList());
        });
    }

    @NonNull
    public LiveData<List<AssetAndPrice>> getTotalAssetAndPrices() {
        MutableLiveData<List<AssetAndPrice>> totalAssetCurrentPrice = new MutableLiveData<>();
        Disposable disposable = Single.fromCallable(() -> {
                    List<Asset> assets = AssetService.getInstance().getAssetsAsync().blockingGet();
                    return assets.stream()
                            .map(asset -> new AssetAndPrice(
                                    asset,
                                    AssetService.getInstance().getAssetCurrentPriceAsync(asset)
                                            .blockingGet())
                            ).collect(Collectors.toList());
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(totalAssetCurrentPrice::setValue);
        compositeDisposable.add(disposable);

        return totalAssetCurrentPrice;
    }

    @Override
    protected void onCleared() {
        compositeDisposable.clear();
        super.onCleared();
    }
}
