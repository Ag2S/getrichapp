package org.alan.getrich.main;

import android.content.Context;
import android.content.Intent;

import androidx.activity.result.contract.ActivityResultContract;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.alan.getrich.asset.Asset;
import org.alan.getrich.asset.AssetSummaryActivity;

public class AssetSummary extends ActivityResultContract<AssetSummary.Input, Void> {
    static class Input {
        private final Asset asset;
        private final Class<?> activityClass;

        private Input(Asset asset, Class<?> activityClass) {
            this.asset = asset;
            this.activityClass = activityClass;
        }
    }

    public static Input view(Asset asset) {
        return new Input(asset, AssetSummaryActivity.class);
    }

    @NonNull
    @Override
    public Intent createIntent(@NonNull Context context, Input input) {
        Intent intent = new Intent(context, input.activityClass);
        intent.putExtra("asset", input.asset);
        return intent;
    }

    @Override
    public Void parseResult(int resultCode, @Nullable Intent intent) {
        return null;
    }
}
