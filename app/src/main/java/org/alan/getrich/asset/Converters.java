package org.alan.getrich.asset;

import androidx.room.TypeConverter;

import java.util.Calendar;
import java.util.Currency;

public class Converters {
    @TypeConverter
    public static Calendar fromTimestamp(Long value) {
        if (value == null) return null;
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(value);
        return calendar;
    }

    @TypeConverter
    public static Long calendarToTimestamp(Calendar calendar) {
        return calendar == null ? null : calendar.getTimeInMillis();
    }

    @TypeConverter
    public static AssetRecord.Type fromOrdinal(Integer value) {
        return value == null ? null : AssetRecord.Type.values()[value];
    }

    @TypeConverter
    public static Integer AssetRecordTypeToOrdinal(AssetRecord.Type type) {
        return type == null ? null : type.ordinal();
    }

    @TypeConverter
    public static AssetInfo.Type AssetInfoTypeFromOrdinal(Integer value) {
        return value == null ? null : AssetInfo.Type.values()[value];
    }

    @TypeConverter
    public static Integer AssetInfoTypeToOrdinal(AssetInfo.Type type) {
        return type == null ? null : type.ordinal();
    }

    @TypeConverter
    public static Currency FromString(String value) {
        return value == null ? null : Currency.getInstance(value);
    }

    @TypeConverter
    public static String AssetInfoTypeToOrdinal(Currency currency) {
        return currency == null ? null : currency.getCurrencyCode();
    }
}
