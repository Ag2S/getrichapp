package org.alan.getrich.asset;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import org.alan.getrich.ReturnUtil;

import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class AssetSummaryViewModel extends ViewModel {
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    public LiveData<List<AssetRecord>> refreshAssetRecords(int assetId) {
        MutableLiveData<List<AssetRecord>> assetRecordsLiveData = new MutableLiveData<>();
        Disposable disposable = AssetService.getInstance().getAssetRecordsByIdAsync(assetId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(assetRecordsLiveData::setValue);
        compositeDisposable.add(disposable);
        return assetRecordsLiveData;
    }

    public LiveData<Asset> refreshAsset(int assetId) {
        MutableLiveData<Asset> assetLiveData = new MutableLiveData<>();
        Disposable disposable = AssetService.getInstance().getAssetByIdAsync(assetId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(assetLiveData::setValue);
        compositeDisposable.add(disposable);
        return assetLiveData;
    }

    public void deleteAsset(Asset asset) {
        Disposable disposable =
                AssetService.getInstance().deleteAssetAsync(asset)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe();
        compositeDisposable.add(disposable);
    }

    public LiveData<Double> refreshXirr(Asset asset) {
        MutableLiveData<Double> xirr = new MutableLiveData<>();
        Disposable disposable = getTotalAssetRecords(asset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(totalAssetRecords -> xirr.setValue(ReturnUtil.getXirr(totalAssetRecords)));
        compositeDisposable.add(disposable);
        return xirr;
    }

    public LiveData<Double> refreshCagr(Asset asset) {
        MutableLiveData<Double> cagr = new MutableLiveData<>();
        Disposable disposable = getTotalAssetRecords(asset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(totalAssetRecords -> cagr.setValue(ReturnUtil.getCagr(totalAssetRecords::stream)));
        compositeDisposable.add(disposable);
        return cagr;
    }


    public LiveData<Double> refreshRateOfReturn(Asset asset) {
        MutableLiveData<Double> rateOfReturn = new MutableLiveData<>();
        Disposable disposable = getTotalAssetRecords(asset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(totalAssetRecords -> rateOfReturn.setValue(ReturnUtil.getRateOfReturn(totalAssetRecords::stream)));
        compositeDisposable.add(disposable);
        return rateOfReturn;
    }

    @NonNull
    private static Single<List<AssetRecord>> getTotalAssetRecords(Asset asset) {
        return Single.fromCallable(() -> Stream.concat(
                AssetService.getInstance().getAssetRecordsByIdAsync(asset.id)
                        .blockingGet()
                        .stream(),
                Stream.of(asset)
                        .filter(current_asset -> current_asset.amounts != 0)
                        .map(current_asset -> AssetRecord.assetRecordWithoutID(current_asset.amounts,
                                AssetService.getInstance().getAssetCurrentPriceAsync(current_asset)
                                        .blockingGet(),
                                Calendar.getInstance(),
                                AssetRecord.Type.SOLD))
        ).collect(Collectors.toList()));
    }

    public LiveData<Double> getAssetPrice(Asset asset) {
        MutableLiveData<Double> price = new MutableLiveData<>();
        Disposable disposable = AssetService.getInstance().getAssetCurrentPriceAsync(asset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(price::postValue);
        compositeDisposable.add(disposable);
        return price;
    }
}
