package org.alan.getrich.asset;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import org.alan.getrich.R;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class AssetRecordDetailActivity extends AppCompatActivity {
    protected AssetRecordDetailViewModel viewModel;
    protected EditText amountEditText;
    protected EditText unitPriceEditText;
    protected AssetRecord assetRecord;
    protected int assetId;
    protected Button updateButton;
    protected Button dateButton;
    private ActivityResultLauncher<AssetRecordDetail.Input> assetRecordLauncher;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asset_record_detail);
        viewModel = new ViewModelProvider(this).get(AssetRecordDetailViewModel.class);
        assetRecord = (AssetRecord) getIntent().getSerializableExtra("assetRecord");
        assetId = getIntent().getIntExtra("assetId", 0);
        TextView assetRecordNameTextView = findViewById(R.id.asset_record_name);
        assetRecordNameTextView.setText(getString(assetRecord.type.getStringRes()));
        TextView amountTextView = findViewById(R.id.amount_text);
        amountTextView.setText(R.string.amount);
        TextView unitPriceTextView = findViewById(R.id.unit_price_text);
        unitPriceTextView.setText(R.string.price);
        dateButton = findViewById(R.id.date);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        dateFormat.format(assetRecord.date.getTime());
        dateButton.setText(dateFormat.format(assetRecord.date.getTime()));
        updateButton = findViewById(R.id.update);
        amountEditText = findViewById(R.id.amount);
        amountEditText.setText(String.format(Locale.getDefault(), "%f", assetRecord.amount));
        unitPriceEditText = findViewById(R.id.unit_price);
        unitPriceEditText.setText(String.format(Locale.getDefault(), "%f", assetRecord.price));
        amountEditText.setEnabled(isEditMode());
        unitPriceEditText.setEnabled(isEditMode());
        updateButton.setVisibility(isEditMode() ? View.VISIBLE : View.INVISIBLE);
        onPostCreate();
    }

    protected boolean isEditMode() {
        return false;
    }

    protected void onPostCreate() {
        assetRecordLauncher =
                registerForActivityResult(new AssetRecordDetail(), resultCode -> {
                    if (resultCode == Activity.RESULT_OK) {
                        finish();
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_asset_record_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.delete) {
            AlertDialog.Builder chooseAssetRecordDialog = new AlertDialog.Builder(this);
            chooseAssetRecordDialog.setTitle(R.string.check_delete);
            chooseAssetRecordDialog.setPositiveButton(R.string.yes, (dialog, which) ->
                    viewModel.deleteAssetRecord(assetId, assetRecord).observe(this, result -> result
                            .onSuccess(v -> {
                                setResult(RESULT_OK);
                                finish();
                            })
                            .onFailure(throwable -> {
                                if (throwable instanceof NegativeAmountException) {
                                    Toast.makeText(this, R.string.delete_asset_record_warning, Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(this, R.string.error_message, Toast.LENGTH_LONG).show();
                                }

                            })));
            chooseAssetRecordDialog.setNegativeButton(R.string.no, null);
            chooseAssetRecordDialog.show();
            return true;
        } else if (itemId == R.id.update) {
            assetRecordLauncher.launch(AssetRecordDetail.update(assetId, assetRecord));
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
}
