package org.alan.getrich.asset;

public class NegativeAmountException extends RuntimeException {
    public NegativeAmountException() {
        super("amount is negative");
    }
}
