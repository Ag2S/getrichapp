package org.alan.getrich.asset;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;

import org.alan.getrich.R;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class InsertSoldAssetRecordActivity extends AppCompatActivity {
    private InsertSoldAssetRecordViewModel viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_sold_asset_record);
        viewModel = new ViewModelProvider(this).get(InsertSoldAssetRecordViewModel.class);

        Asset asset = (Asset) getIntent().getSerializableExtra("asset");
        TextView nameText = findViewById(R.id.asset_name);
        nameText.setText(asset.assetInfo.name);
        EditText priceEditView = findViewById(R.id.unit_sold_price);
        EditText amountEditView = findViewById(R.id.amount);
        Button dateButton = findViewById(R.id.date);
        viewModel.calendarLiveData.observe(this, calendar -> {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            dateButton.setText(dateFormat.format(calendar.getTime()));
        });
        dateButton.setOnClickListener(view -> {
            DialogFragment newFragment = new InsertSoldAssetRecordDatePickerFragment();
            newFragment.show(getSupportFragmentManager(), "datePicker");
        });
        Button insertNewAssetRecordButton = findViewById(R.id.insertNewAssetRecordButton);
        insertNewAssetRecordButton.setOnClickListener(view -> {
            if (amountEditView.getText().toString().isEmpty()) {
                Toast.makeText(this, R.string.insert_amount_warning, Toast.LENGTH_LONG).show();
            } else if (priceEditView.getText().toString().isEmpty()) {
                Toast.makeText(this, R.string.insert_price_warning, Toast.LENGTH_LONG).show();
            } else if (viewModel.calendarLiveData.getValue() == null) {
                Toast.makeText(this, R.string.insert_date_warning, Toast.LENGTH_LONG).show();
            } else {
                AssetRecord assetRecord = new AssetRecord(
                        Double.parseDouble(amountEditView.getText().toString()),
                        Double.parseDouble(priceEditView.getText().toString()),
                        viewModel.calendarLiveData.getValue(),
                        AssetRecord.Type.SOLD, asset.id);
                viewModel.insertSoldAssetRecordAsync(assetRecord, asset.id).observe(this,
                        result -> result
                                .onSuccess(v -> this.finish())
                                .onFailure(throwable -> {
                                    if (throwable.getClass() == NegativeAmountException.class) {
                                        Toast.makeText(this, R.string.insert_sold_asset_record_warning, Toast.LENGTH_LONG).show();
                                    } else {
                                        Toast.makeText(this, R.string.error_message, Toast.LENGTH_LONG).show();
                                    }
                                })
                );
            }
        });
    }
}
