package org.alan.getrich.asset;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.alan.getrich.R;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.function.Consumer;

public class AssetSummaryRecyclerViewAdapter extends RecyclerView.Adapter<AssetSummaryRecyclerViewAdapter.ViewHolder> {
    private final Context context;
    private final List<AssetRecord> assetRecords;
    private final Consumer<AssetRecord> onAssetRecordClickedListener;

    public AssetSummaryRecyclerViewAdapter(Context context, List<AssetRecord> assetRecords, Consumer<AssetRecord> onAssetRecordClickedListener) {
        this.context = context;
        this.assetRecords = assetRecords;
        this.onAssetRecordClickedListener = onAssetRecordClickedListener;
    }

    @NonNull
    @Override
    public AssetSummaryRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.asset_record_layout_for_summary, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AssetSummaryRecyclerViewAdapter.ViewHolder holder, int position) {
        AssetRecord currentAssetRecord = assetRecords.get(position);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        String setTextString = context.getString(currentAssetRecord.type.getStringRes()) + "," +
                currentAssetRecord.amount + "," +
                currentAssetRecord.price + "," +
                dateFormat.format(currentAssetRecord.date.getTime());
        holder.assetRecord.setText(setTextString);
        holder.assetRecord.setOnClickListener(view -> onAssetRecordClickedListener.accept(currentAssetRecord));
    }

    @Override
    public int getItemCount() {
        return assetRecords.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        Button assetRecord;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            assetRecord = itemView.findViewById(R.id.asset_record_for_summary);
        }
    }
}
