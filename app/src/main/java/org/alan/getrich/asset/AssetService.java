package org.alan.getrich.asset;

import androidx.room.Room;

import org.alan.getrich.GetRichApplication;
import org.alan.getrich.asset.finance.FinanceDao;

import java.util.List;

import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Single;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class AssetService {
    private static final AssetService INSTANCE = new AssetService();
    private final GetRichDao getRichDao;
    private final FinanceDao financeDao;

    private AssetService() {
        getRichDao = Room.databaseBuilder(
                GetRichApplication.context,
                AssetDatabase.class,
                "database-name"
        ).build().assetRecordDao();
        financeDao = new Retrofit.Builder()
                .baseUrl("https://query1.finance.yahoo.com")
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(FinanceDao.class);
    }

    public static AssetService getInstance() {
        return INSTANCE;
    }


    public Completable insertBoughtAssetRecordAsync(int assetId, AssetRecord assetRecord) {
        return Completable.fromAction(() -> {
            Asset targetAsset = getRichDao.getAssetById(assetId).blockingGet();
            getRichDao.updateAssetAmount(targetAsset.id, targetAsset.amounts + assetRecord.amount)
                    .blockingAwait();
            getRichDao.insertAssetRecord(assetRecord)
                    .blockingAwait();
        });
    }

    public Completable insertSoldAssetRecordAsync(int assetId, AssetRecord assetRecord) {
        return Completable.fromAction(() -> {
            Asset targetAsset = getRichDao.getAssetById(assetId).blockingGet();
            if (targetAsset.amounts - assetRecord.amount < 0) {
                throw new NegativeAmountException();
            }
            getRichDao.updateAssetAmount(targetAsset.id, targetAsset.amounts - assetRecord.amount)
                    .blockingAwait();
            getRichDao.insertAssetRecord(assetRecord)
                    .blockingAwait();
        });
    }

    public Completable insertDividendAssetRecordAsync(AssetRecord assetRecord) {
        return Completable.fromAction(getRichDao.insertAssetRecord(assetRecord)::blockingAwait);
    }

    public Completable deleteAssetRecordAsync(int assetId, AssetRecord deletingAssetRecord) {
        return Completable.fromAction(() -> {
            Asset targetAsset = getRichDao.getAssetById(assetId).blockingGet();
            if (targetAsset == null) {
                throw new RuntimeException("Target asset not found");
            }
            double currentAssetAmount = targetAsset.amounts;
            switch (deletingAssetRecord.type) {
                case BOUGHT:
                    currentAssetAmount = currentAssetAmount - deletingAssetRecord.amount;
                    if (currentAssetAmount < 0) {
                        throw new NegativeAmountException();
                    }
                    targetAsset.amounts = currentAssetAmount;
                    break;
                case SOLD:
                    targetAsset.amounts = currentAssetAmount + deletingAssetRecord.amount;
                case DIVIDEND:
                    break;
                default:
                    throw new RuntimeException("asset record type not found");
            }
            getRichDao.deleteRecord(deletingAssetRecord).blockingAwait();
        });

    }


    public Completable updateAssetRecordAsync(int assetId, AssetRecord updatingAssetRecord) {
        return Completable.fromAction(() -> {
            Asset targetAsset = getRichDao.getAssetById(assetId).blockingGet();
            double currentAssetAmount = targetAsset.amounts;
            AssetRecord targetAssetRecord = getRichDao.getAssetRecordById(updatingAssetRecord.id).blockingGet();
            if (targetAssetRecord == null) {
                throw new RuntimeException("Asset record id not found in DB.");
            }
            switch (updatingAssetRecord.type) {
                case BOUGHT:
                    currentAssetAmount = currentAssetAmount - targetAssetRecord.amount + updatingAssetRecord.amount;
                    if (currentAssetAmount < 0) {
                        throw new NegativeAmountException();
                    }
                    getRichDao.updateAssetAmount(assetId, currentAssetAmount).blockingAwait();
                    break;
                case SOLD:
                    currentAssetAmount = currentAssetAmount + targetAssetRecord.amount - updatingAssetRecord.amount;
                    if (currentAssetAmount < 0) {
                        throw new NegativeAmountException();
                    }
                    getRichDao.updateAssetAmount(assetId, currentAssetAmount).blockingAwait();
                    break;
                case DIVIDEND:
                    if (updatingAssetRecord.amount != 0) {
                        throw new DividendNotZeroAmountException();
                    }
                    break;
                default:
                    throw new RuntimeException("asset record type not found");
            }
            getRichDao.updateRecord(updatingAssetRecord).blockingAwait();
        });
    }

    public Single<List<AssetRecord>> getAssetRecordsByIdAsync(int assetId) {
        return getRichDao.getAssetRecordsById(assetId);
    }

    public Single<List<AssetRecord>> getAssetRecordsAsync() {
        return getRichDao.getAssetRecords();
    }

    public Single<List<Asset>> getAssetsAsync() {
        return getRichDao.getAssets();
    }

    public Single<Asset> getAssetByIdAsync(int assetId) {
        return getRichDao.getAssetById(assetId);
    }

    public Completable deleteAssetAsync(Asset asset) {
        return getRichDao.deleteAsset(asset);
    }

    public Completable insertAssetAsync(Asset asset) {
        return getRichDao.insertAsset(asset);
    }

    public Single<Double> getAssetCurrentPriceAsync(Asset asset) {
        return Single.fromCallable(() -> {
            if (asset.assetInfo.type == AssetInfo.Type.ETF |
                    asset.assetInfo.type == AssetInfo.Type.STOCK |
                    asset.assetInfo.type == AssetInfo.Type.FUTURES) {
                return financeDao.getFinance(asset.assetInfo.financeCodeName).blockingGet().chart.result.get(0).meta.regularMarketPrice;
            }else {
                return Double.valueOf(asset.id);
            }
        });
    }
}
