package org.alan.getrich.asset;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import org.alan.getrich.Result;

import java.util.Calendar;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class AssetRecordDetailViewModel extends ViewModel {
    private final MutableLiveData<Calendar> _calendarMutableLiveData = new MutableLiveData<>();
    public LiveData<Calendar> calendarLiveData = _calendarMutableLiveData;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    public LiveData<Result<Void>> updateAssetRecord(int assetId, AssetRecord updateAssetRecord) {
        MutableLiveData<Result<Void>> result = new MutableLiveData<>();
        Disposable disposable = AssetService.getInstance().updateAssetRecordAsync(assetId, updateAssetRecord)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> result.setValue(Result.success(null)),
                        throwable -> result.setValue(Result.failure(throwable)));
        compositeDisposable.add(disposable);
        return result;
    }

    public LiveData<Result<Void>> deleteAssetRecord(int assetId, AssetRecord deleteAssetRecord) {
        MutableLiveData<Result<Void>> result = new MutableLiveData<>();
        Disposable disposable = AssetService.getInstance().deleteAssetRecordAsync(assetId, deleteAssetRecord)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> result.setValue(Result.success(null)),
                        throwable -> result.setValue(Result.failure(throwable)));
        compositeDisposable.add(disposable);
        return result;
    }

    public void setCalendar(Calendar newCalendar) {
        _calendarMutableLiveData.postValue(newCalendar);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.clear();
    }
}
