package org.alan.getrich.asset;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.Calendar;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class InsertDividendAssetRecordViewModel extends ViewModel {
    private final MutableLiveData<Calendar> _calendarMutableLiveData = new MutableLiveData<>();
    public LiveData<Calendar> calendarLiveData = _calendarMutableLiveData;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();


    public void saveAssetRecord(AssetRecord assetRecord) {
        Disposable disposable = AssetService.getInstance().insertDividendAssetRecordAsync(assetRecord)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
        compositeDisposable.add(disposable);
    }

    public void setCalendar(Calendar newCalendar) {
        _calendarMutableLiveData.postValue(newCalendar);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.clear();
    }
}
