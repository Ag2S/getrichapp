package org.alan.getrich.asset;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import org.alan.getrich.R;

import java.io.Serializable;
import java.util.Calendar;

@Entity(
        foreignKeys = {@ForeignKey(
                entity = Asset.class,
                parentColumns = {"id"},
                childColumns = {"assetId"},
                onUpdate = ForeignKey.CASCADE)}
)
public class AssetRecord implements Serializable {
    @Ignore
    public static final int INVALID_ID = -1;
    public Type type;// 自訂enum
    public double amount;
    public double price;
    public Calendar date;
    @PrimaryKey(autoGenerate = true)
    public int id;
    public int assetId;

    public enum Type {
        BOUGHT {
            @Override
            public int getStringRes() {
                return R.string.bought_asset_record;
            }
        },
        SOLD {
            @Override
            public int getStringRes() {
                return R.string.sold_asset_record;
            }
        },
        DIVIDEND {
            @Override
            public int getStringRes() {
                return R.string.dividend_asset_record;
            }
        };

        public abstract int getStringRes();

    }

    public AssetRecord(double amount, double price, Calendar date, AssetRecord.Type type, int assetId) {
        this.amount = amount;
        this.price = price;
        this.date = date;
        this.type = type;
        this.assetId = assetId;
    }

    public static AssetRecord assetRecordWithoutID(double amount, double price, Calendar date, Type type) {
        return new AssetRecord(amount, price, date, type, INVALID_ID);
    }
}
