package org.alan.getrich.asset;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class Asset implements Serializable {
    @PrimaryKey(autoGenerate = true)
    public int id;
    @Embedded
    public AssetInfo assetInfo;
    public double amounts;

    public Asset(AssetInfo assetInfo, double amounts) {
        this.assetInfo = assetInfo;
        this.amounts = amounts;
    }
}
