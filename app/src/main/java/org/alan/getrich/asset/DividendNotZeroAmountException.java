package org.alan.getrich.asset;

public class DividendNotZeroAmountException extends RuntimeException {
    public DividendNotZeroAmountException() {
        super("amount is not zero");
    }
}
