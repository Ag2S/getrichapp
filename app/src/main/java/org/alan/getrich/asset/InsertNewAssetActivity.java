package org.alan.getrich.asset;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import org.alan.getrich.R;

import java.util.Arrays;
import java.util.Currency;
import java.util.List;
import java.util.stream.Collectors;

public class InsertNewAssetActivity extends AppCompatActivity {
    private InsertNewAssetViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_new_asset);
        viewModel = new ViewModelProvider(InsertNewAssetActivity.this).get(InsertNewAssetViewModel.class);
        Spinner typeAssetSpinner = initializeSpinner(R.id.typeOfAssetSpinner,
                Arrays.stream(AssetInfo.Type.values())
                        .map(type -> getString(type.getStringRes()))
                        .collect(Collectors.toList()));
        Spinner currencySpinner = initializeSpinner(R.id.currencySpinner,
                Constants.SUPPORTED_CURRENCIES.stream()
                        .map(Currency::getDisplayName)
                        .collect(Collectors.toList()));
        Button newAssetButton = findViewById(R.id.insertNewAssetButton);
        newAssetButton.setOnClickListener(view -> {
            AssetInfo assetInfo = viewModel.getCurrentSelectedAsset().getValue();

            viewModel.saveAssetData(assetInfo);
            finish();
        });
//        todo: make a interface for two spinner listener
        typeAssetSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                AssetInfo.Type typeOfAsset = AssetInfo.Type.values()[position];
                Currency currency = Constants.SUPPORTED_CURRENCIES.get(currencySpinner.getSelectedItemPosition());
                if (typeOfAsset != (AssetInfo.Type.DEPOSIT_INSURANCE)) {
                    viewModel.refreshAssetNames(typeOfAsset, currency)
                            .observe(InsertNewAssetActivity.this, InsertNewAssetActivity.this::setFragmentByAssets);
                } else {
                    setFragmentForOthers();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        currencySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                AssetInfo.Type typeOfAsset = AssetInfo.Type.values()[typeAssetSpinner.getSelectedItemPosition()];
                Currency currency = Constants.SUPPORTED_CURRENCIES.get(position);
                if (typeOfAsset != (AssetInfo.Type.DEPOSIT_INSURANCE)) {
                    viewModel.refreshAssetNames(typeOfAsset, currency)
                            .observe(InsertNewAssetActivity.this, InsertNewAssetActivity.this::setFragmentByAssets);
                } else {
                    setFragmentForOthers();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setFragmentByAssets(List<AssetInfo> assetInfo) {
        InsertNewAssetFragment fragment = new InsertNewAssetFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.newAssetFragmentContainer, fragment);
        fragmentTransaction.commit();
    }

    private Spinner initializeSpinner(int spinnerID, List<String> stringList) {
        Spinner spinner = findViewById(spinnerID);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, stringList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        return spinner;
    }

    private void setFragmentForOthers() {
        InsertNewAssetViewFragmentForOther fragment = new InsertNewAssetViewFragmentForOther();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.newAssetFragmentContainer, fragment);
        fragmentTransaction.commit();
    }
}
