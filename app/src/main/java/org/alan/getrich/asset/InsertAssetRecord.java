package org.alan.getrich.asset;

import android.content.Context;
import android.content.Intent;

import androidx.activity.result.contract.ActivityResultContract;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class InsertAssetRecord extends ActivityResultContract<InsertAssetRecord.Input, Void> {
    static class Input {
        private final Asset asset;
        private final Class<?> activityClass;

        Input(Asset asset, Class<?> activityClass) {
            this.asset = asset;
            this.activityClass = activityClass;
        }
    }

    @NonNull
    @Override
    public Intent createIntent(@NonNull Context context, Input input) {
        Intent intent = new Intent(context, input.activityClass);
        intent.putExtra("asset", input.asset);
        return intent;
    }

    @Override
    public Void parseResult(int resultCode, @Nullable Intent intent) {
        return null;
    }
}
