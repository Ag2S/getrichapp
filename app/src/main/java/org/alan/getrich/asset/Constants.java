package org.alan.getrich.asset;

import java.util.Arrays;
import java.util.Currency;
import java.util.List;

public class Constants {
    public static List<Currency> SUPPORTED_CURRENCIES =
            Arrays.asList(Currency.getInstance("USD"), Currency.getInstance("TWD"),
                    Currency.getInstance("JPY"));
}
