package org.alan.getrich.asset;

public class AssetAndPrice {
    public Asset asset;
    public double price;

    public AssetAndPrice(Asset asset, double price) {
        this.asset = asset;
        this.price = price;
    }
}
