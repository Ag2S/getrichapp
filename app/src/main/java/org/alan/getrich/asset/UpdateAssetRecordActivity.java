package org.alan.getrich.asset;

import android.view.Menu;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;

import org.alan.getrich.R;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class UpdateAssetRecordActivity extends AssetRecordDetailActivity {

    @Override
    protected void onPostCreate() {
        viewModel.setCalendar(assetRecord.date);
        dateButton.setOnClickListener(view -> {
            DialogFragment newFragment = new UpdateAssetRecordDatePickerFragment();
            newFragment.show(getSupportFragmentManager(), "datePicker");
        });
        viewModel.calendarLiveData.observe(this, calendar -> {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            dateButton.setText(dateFormat.format(calendar.getTime()));
        });
        updateButton.setOnClickListener(view -> {
            assetRecord.amount = Double.parseDouble(amountEditText.getText().toString());
            assetRecord.price = Double.parseDouble(unitPriceEditText.getText().toString());
            assetRecord.date = viewModel.calendarLiveData.getValue();
            viewModel.updateAssetRecord(assetId, assetRecord).observe(this, result -> result
                    .onSuccess(v -> {
                        setResult(RESULT_OK);
                        finish();
                    })
                    .onFailure(throwable -> {
                        if (throwable instanceof NegativeAmountException) {
                            Toast.makeText(this, R.string.update_asset_record_warning, Toast.LENGTH_LONG).show();
                        } else if (throwable instanceof DividendNotZeroAmountException) {
                            Toast.makeText(this, R.string.update_dividend_asset_record_warning, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(this, R.string.error_message, Toast.LENGTH_LONG).show();
                        }
                    }));
        });
    }

    @Override
    protected boolean isEditMode() {
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }
}
