package org.alan.getrich.asset;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import org.alan.getrich.R;


public class InsertNewAssetViewFragmentForOther extends Fragment {
    private InsertNewAssetViewModel viewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_insert_new_asset_others, container, false);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        viewModel = new ViewModelProvider(requireActivity()).get(InsertNewAssetViewModel.class);
    }

    @Override
    public void onResume() {
        super.onResume();
        EditText editText = requireActivity().findViewById(R.id.asset_name_for_others);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                viewModel.selectAsset(new AssetInfo(AssetInfo.Type.DEPOSIT_INSURANCE, Constants.SUPPORTED_CURRENCIES.get(0), s.toString(), "NotDefined"));
            }
        });
    }
}
