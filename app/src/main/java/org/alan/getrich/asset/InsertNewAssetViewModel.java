package org.alan.getrich.asset;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class InsertNewAssetViewModel extends ViewModel {
    private MutableLiveData<List<AssetInfo>> assets;
    private final MutableLiveData<AssetInfo> currentSelectedAsset = new MutableLiveData<>();
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    public void selectAsset(AssetInfo assetInfo) {
        currentSelectedAsset.postValue(assetInfo);
    }

    public LiveData<AssetInfo> getCurrentSelectedAsset() {
        return currentSelectedAsset;
    }

    public List<AssetInfo> loadAssets() {
        return assets.getValue();
    }

    public void saveAssetData(AssetInfo assetInfo) {
        Asset insertAsset = new Asset(assetInfo, 0);
        Disposable disposable = AssetService.getInstance().insertAssetAsync(insertAsset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
        compositeDisposable.add(disposable);
    }

    public LiveData<List<AssetInfo>> refreshAssetNames(AssetInfo.Type typeOfAsset, Currency currency) {
        if (assets == null) {
            assets = new MutableLiveData<>();
        }
        loadAssets(typeOfAsset, currency);
        return assets;
    }

    private void loadAssets(AssetInfo.Type typeOfAsset, Currency currency) {
        switch (typeOfAsset) {
            case STOCK: {
                List<AssetInfo> testList = new ArrayList<>();
                testList.add(new AssetInfo(typeOfAsset, currency, "台積電", "2330.TW"));
                testList.add(new AssetInfo(typeOfAsset, currency, "大立光", "3008.TW"));
                testList.add(new AssetInfo(typeOfAsset, currency, "聯電", "2303.TW"));
                assets.postValue(testList);
                break;
            }
            case FUTURES: {
                List<AssetInfo> testList = new ArrayList<>();
                testList.add(new AssetInfo(typeOfAsset, currency, "臺股期貨", "TX"));
                testList.add(new AssetInfo(typeOfAsset, currency, "東證期貨", "TOPIX"));
                testList.add(new AssetInfo(typeOfAsset, currency, "美國道瓊期貨", "DJIA"));
                assets.postValue(testList);
                break;
            }
            case OPTION: {
                List<AssetInfo> testList = new ArrayList<>();
                testList.add(new AssetInfo(typeOfAsset, currency, "臺指選擇權", "NotDefined"));
                testList.add(new AssetInfo(typeOfAsset, currency, "黃金選擇權", "NotDefined"));
                testList.add(new AssetInfo(typeOfAsset, currency, "電子選擇權", "NotDefined"));
                assets.postValue(testList);
                break;
            }
            case ETF: {
                List<AssetInfo> testList = new ArrayList<>();
                testList.add(new AssetInfo(typeOfAsset, currency, "0050", "0050.TW"));
                testList.add(new AssetInfo(typeOfAsset, currency, "006208", "006208.TW"));
                testList.add(new AssetInfo(typeOfAsset, currency, "0056", "0056.TW"));
                assets.postValue(testList);
                break;
            }
            case BOND: {
                List<AssetInfo> testList = new ArrayList<>();
                testList.add(new AssetInfo(typeOfAsset, currency, "台灣公債2年期", "NotDefined"));
                testList.add(new AssetInfo(typeOfAsset, currency, "102央債甲8", "NotDefined"));
                testList.add(new AssetInfo(typeOfAsset, currency, "101央債甲2", "NotDefined"));
                assets.postValue(testList);
                break;
            }
            case FUND: {
                List<AssetInfo> testList = new ArrayList<>();
                testList.add(new AssetInfo(typeOfAsset, currency, "富蘭克林華美全球成長基金", "NotDefined"));
                testList.add(new AssetInfo(typeOfAsset, currency, "富蘭克林華美全球投資級債券基金", "NotDefined"));
                testList.add(new AssetInfo(typeOfAsset, currency, "富蘭克林華美全球高收益債券基金", "NotDefined"));
                assets.postValue(testList);
                break;
            }
            default: {
                List<AssetInfo> testList = new ArrayList<>();
                assets.postValue(testList);
                break;
            }
        }
    }
}
