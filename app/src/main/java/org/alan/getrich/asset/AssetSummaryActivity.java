package org.alan.getrich.asset;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import org.alan.getrich.R;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;


public class AssetSummaryActivity extends AppCompatActivity {
    private AssetSummaryViewModel viewModel;
    private RecyclerView recyclerView;
    private ActivityResultLauncher<InsertAssetRecord.Input> insertRecordLauncher;
    private ActivityResultLauncher<AssetRecordDetail.Input> assetRecordLauncher;
    private Asset asset;
    private TextView assetNameTextView;
    private TextView assetAmountTextView;
    private TextView assetPriceTextView;
    TextView cagrTextview;
    TextView rateOfReturnTextview;
    TextView xirrTextview;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asset_summary);
        asset = (Asset) getIntent().getSerializableExtra("asset");
        viewModel = new ViewModelProvider(this).get(AssetSummaryViewModel.class);
        recyclerView = findViewById(R.id.asset_record_list);
        TextView nameTextView = findViewById(R.id.asset_name_text);
        TextView priceTextView = findViewById(R.id.asset_price_text);
        TextView amountTextView = findViewById(R.id.asset_amount_text);
        assetNameTextView = findViewById(R.id.asset_name);
        assetAmountTextView = findViewById(R.id.asset_amount);
        assetPriceTextView = findViewById(R.id.asset_price);
        cagrTextview = findViewById(R.id.cagr);
        rateOfReturnTextview = findViewById(R.id.rate_of_return);
        xirrTextview = findViewById(R.id.xirr);
        Button insertNewAssetRecordButton = findViewById(R.id.new_asset_record_button);
        Button deleteAssetButton = findViewById(R.id.delete_asset_button);

        nameTextView.setText(getString(R.string.asset));
        priceTextView.setText(getString(R.string.price));
        amountTextView.setText(getString(R.string.amount));

        viewModel.getAssetPrice(asset).observe(this, price -> {
            assetNameTextView.setText(asset.assetInfo.name);
            assetPriceTextView.setText(String.format(String.format(Locale.getDefault(), "%f", price)));
            assetAmountTextView.setText(String.format(String.format(Locale.getDefault(), "%f", asset.amounts)));
        });

        viewModel.refreshAssetRecords(asset.id).observe(this, this::onAssetRecordsRefreshed);

        assetRecordLauncher = registerForActivityResult(new AssetRecordDetail(),
                unused -> refreshAssetAndTextView(asset));

        insertRecordLauncher = registerForActivityResult(new InsertAssetRecord(),
                unused -> refreshAssetAndTextView(asset));
        String[] assetRecordTypeStrings = new String[]{
                getString(R.string.bought_asset_record),
                getString(R.string.sold_asset_record),
                getString(R.string.dividend_asset_record)};
        AlertDialog.Builder chooseAssetRecordDialog = new AlertDialog.Builder(this)
                .setTitle(R.string.pick_asset_record_type)
                .setItems(assetRecordTypeStrings, (dialog, which) -> {
                    switch (which) {
                        case 0:
                            insertRecordLauncher.launch(new InsertAssetRecord.Input(asset, InsertBoughtAssetRecordActivity.class));
                            break;
                        case 1:
                            insertRecordLauncher.launch(new InsertAssetRecord.Input(asset, InsertSoldAssetRecordActivity.class));
                            break;
                        case 2:
                            insertRecordLauncher.launch(new InsertAssetRecord.Input(asset, InsertDividendAssetRecordActivity.class));
                            break;
                    }
                });
        insertNewAssetRecordButton.setOnClickListener(view -> chooseAssetRecordDialog.show());

        deleteAssetButton.setOnClickListener(view -> {
            String[] checkStrings = new String[]{
                    getString(R.string.yes),
                    getString(R.string.no),};
            AlertDialog.Builder deleteAssetDialog = new AlertDialog.Builder(this)
                    .setTitle(R.string.check_delete)
                    .setItems(checkStrings, (dialog, which) -> {
                        switch (which) {
                            case 0:
                                viewModel.deleteAsset(asset);
                                finish();
                                break;
                            case 1:
                                break;
                        }
                    });
            deleteAssetDialog.show();
        });

        refreshAssetDetailNumbers();
    }

    private void refreshAssetDetailNumbers() {
        viewModel.refreshXirr(asset).observe(this, xirr -> {
            DecimalFormat decimalFormat = new DecimalFormat("##.##");
            String xirrString = getString(R.string.xirr) + decimalFormat.format(xirr * 100) + "%";
            xirrTextview.setText(xirrString);
        });
        viewModel.refreshCagr(asset).observe(this, cagr -> {
            DecimalFormat decimalFormat = new DecimalFormat("##.##");
            String cagrString = getString(R.string.cagr) + decimalFormat.format(cagr * 100) + "%";
            cagrTextview.setText(cagrString);
        });
        viewModel.refreshRateOfReturn(asset).observe(this, rateOfReturn -> {
            DecimalFormat decimalFormat = new DecimalFormat("##.##");
            String rateOfReturnString = getString(R.string.rateOfReturn) + decimalFormat.format(rateOfReturn * 100) + "%";
            rateOfReturnTextview.setText(rateOfReturnString);
        });
    }

    private void onAssetRecordsRefreshed(List<AssetRecord> assetRecords) {
        recyclerView.setAdapter(
                new AssetSummaryRecyclerViewAdapter(
                        this, assetRecords, this::onAssetRecordClicked));
    }

    private void onAssetRefreshed(Asset refreshedAsset) {
        viewModel.getAssetPrice(asset).observe(this, price -> {
            assetNameTextView.setText(asset.assetInfo.name);
            assetPriceTextView.setText(String.format(String.format(Locale.getDefault(), "%f", price)));
            assetAmountTextView.setText(String.format(String.format(Locale.getDefault(), "%f", asset.amounts)));
        });
    }

    private void refreshAssetAndTextView(Asset asset) {
        viewModel.refreshAssetRecords(asset.id).observe(this, this::onAssetRecordsRefreshed);
        viewModel.refreshAsset(asset.id).observe(this, this::onAssetRefreshed);
        refreshAssetDetailNumbers();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void onAssetRecordClicked(AssetRecord assetRecord) {
        assetRecordLauncher.launch(AssetRecordDetail.view(asset.id, assetRecord));
    }
}
