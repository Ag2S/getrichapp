package org.alan.getrich.asset;

import org.alan.getrich.R;

import java.io.Serializable;
import java.util.Currency;

public class AssetInfo implements Serializable {
    public Type type;// 自訂enum
    public Currency currency;
    public String name;
    public String financeCodeName;


    public enum Type {
        STOCK {
            @Override
            public int getStringRes() {
                return R.string.stock;
            }
        },
        FUTURES {
            @Override
            public int getStringRes() {
                return R.string.futures;
            }
        },
        OPTION {
            @Override
            public int getStringRes() {
                return R.string.option;
            }
        },
        ETF {
            @Override
            public int getStringRes() {
                return R.string.etf;
            }
        },
        BOND {
            @Override
            public int getStringRes() {
                return R.string.bond;
            }
        },
        FUND {
            @Override
            public int getStringRes() {
                return R.string.fund;
            }
        },
        DEPOSIT_INSURANCE {
            @Override
            public int getStringRes() {
                return R.string.deposit_insurance;
            }
        };

        public abstract int getStringRes();

    }


    public AssetInfo(Type type, Currency currency, String name, String financeCodeName) {
        this.type = type;
        this.currency = currency;
        this.name = name;
        this.financeCodeName = financeCodeName;
    }
}

