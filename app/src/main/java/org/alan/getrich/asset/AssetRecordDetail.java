package org.alan.getrich.asset;

import android.content.Context;
import android.content.Intent;

import androidx.activity.result.contract.ActivityResultContract;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class AssetRecordDetail extends ActivityResultContract<AssetRecordDetail.Input, Integer> {
    static class Input {
        private final int assetID;
        private final AssetRecord assetRecord;
        private final Class<?> activityClass;

        private Input(int assetID, AssetRecord assetRecord, Class<?> activityClass) {
            this.assetID = assetID;
            this.assetRecord = assetRecord;
            this.activityClass = activityClass;
        }

    }

    static public Input view(int assetID, AssetRecord assetRecord) {
        return new Input(assetID, assetRecord, AssetRecordDetailActivity.class);
    }

    static public Input update(int assetID, AssetRecord assetRecord) {
        return new Input(assetID, assetRecord, UpdateAssetRecordActivity.class);
    }

    @NonNull
    @Override
    public Intent createIntent(@NonNull Context context, Input input) {
        Intent intent = new Intent(context, input.activityClass);
        intent.putExtra("assetId", input.assetID);
        intent.putExtra("assetRecord", input.assetRecord);
        return intent;
    }

    @Override
    public Integer parseResult(int resultCode, @Nullable Intent intent) {
        return resultCode;
    }
}
