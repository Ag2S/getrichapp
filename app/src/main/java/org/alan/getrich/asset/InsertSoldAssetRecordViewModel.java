package org.alan.getrich.asset;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import org.alan.getrich.Result;

import java.util.Calendar;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class InsertSoldAssetRecordViewModel extends ViewModel {
    private final MutableLiveData<Calendar> _calendarMutableLiveData = new MutableLiveData<>();
    public LiveData<Calendar> calendarLiveData = _calendarMutableLiveData;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();


    public LiveData<Result<Void>> insertSoldAssetRecordAsync(AssetRecord assetRecord, int assetId) {
        MutableLiveData<Result<Void>> result = new MutableLiveData<>();
        Disposable disposable = AssetService.getInstance().insertSoldAssetRecordAsync(assetId, assetRecord)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> result.setValue(Result.success(null)),
                        throwable -> result.setValue(Result.failure(throwable)));
        compositeDisposable.add(disposable);
        return result;
    }

    public void setCalendar(Calendar newCalendar) {
        _calendarMutableLiveData.postValue(newCalendar);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.clear();
    }
}