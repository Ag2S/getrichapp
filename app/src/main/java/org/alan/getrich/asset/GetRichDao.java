package org.alan.getrich.asset;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Single;

@Dao
public interface GetRichDao {
    @Insert
    Completable insertAssetRecord(AssetRecord assetRecord);

    @Query("SELECT * FROM AssetRecord WHERE AssetId IN (:assetId)")
    Single<List<AssetRecord>> getAssetRecordsById(int assetId);

    @Query("SELECT * FROM AssetRecord WHERE id IN (:id)")
    Single<AssetRecord> getAssetRecordById(int id);

    @Query("SELECT * FROM AssetRecord")
    Single<List<AssetRecord>> getAssetRecords();

    @Update
    Completable updateRecord(AssetRecord assetRecord);

    @Delete
    Completable deleteRecord(AssetRecord assetRecord);

    @Insert
    Completable insertAsset(Asset asset);

    @Query("SELECT * FROM Asset")
    Single<List<Asset>> getAssets();

    @Query("SELECT * FROM Asset WHERE id IN (:id)")
    Single<Asset> getAssetById(int id);

    @Delete
    Completable deleteAsset(Asset asset);

    @Query("UPDATE Asset SET amounts=(:amounts) WHERE id=(:id)")
    Completable updateAssetAmount(int id, double amounts);
}
