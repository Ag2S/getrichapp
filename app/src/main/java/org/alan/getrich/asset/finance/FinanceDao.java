package org.alan.getrich.asset.finance;

import io.reactivex.rxjava3.core.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface FinanceDao {
    @GET("/v8/finance/chart/{finance}?interval=1d")
    Single<FinanceJson> getFinance(@Path("finance") String finance);
}
