package org.alan.getrich.asset;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@Database(entities = {AssetRecord.class, Asset.class}, version = 1)
@TypeConverters({Converters.class})
public abstract class AssetDatabase extends RoomDatabase {
    public abstract GetRichDao assetRecordDao();

}
