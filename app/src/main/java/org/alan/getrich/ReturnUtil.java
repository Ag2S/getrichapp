package org.alan.getrich;

import org.alan.getrich.asset.AssetRecord;
import org.decampo.xirr.OverflowException;
import org.decampo.xirr.Transaction;
import org.decampo.xirr.Xirr;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReturnUtil {
    public static double getCagr(Supplier<Stream<AssetRecord>> assetRecordsStreamSupplier) {
        double endValue = assetRecordsStreamSupplier.get()
                .filter(assetRecord -> assetRecord.type == AssetRecord.Type.SOLD)
                .mapToDouble(assetRecord -> assetRecord.amount * assetRecord.price)
                .sum();
        double initialValue = assetRecordsStreamSupplier.get()
                .filter(assetRecord -> assetRecord.type != AssetRecord.Type.SOLD)
                .mapToDouble(ReturnUtil::getPrice)
                .sum();
        Calendar initialDay = assetRecordsStreamSupplier.get()
                .map(assetRecord -> assetRecord.date)
                .min(Calendar::compareTo)
                .orElse(null);
        Calendar endDay = assetRecordsStreamSupplier.get()
                .map(assetRecord -> assetRecord.date)
                .max(Calendar::compareTo)
                .orElse(null);
        if (initialDay == null) {
            return Double.NaN;
        }
        if (endDay == null) {
            return Double.NaN;
        }
        return (Math.pow((endValue / initialValue),
                (1. / (TimeUnit.MILLISECONDS.toDays(
                        endDay.getTime().getTime() - initialDay.getTime().getTime()
                ) / 365.))) - 1);
    }

    public static double getPrice(AssetRecord assetRecord) {
        if (assetRecord.type == AssetRecord.Type.BOUGHT) {
            return assetRecord.amount * assetRecord.price;
        } else {
            return assetRecord.price;
        }
    }

    public static double getRateOfReturn(Supplier<Stream<AssetRecord>> assetRecordsStreamSupplier) {
        double endValue = assetRecordsStreamSupplier.get()
                .filter(assetRecord -> assetRecord.type == AssetRecord.Type.SOLD)
                .mapToDouble(assetRecord -> assetRecord.amount * assetRecord.price)
                .sum();
        double initialValue = assetRecordsStreamSupplier.get()
                .filter(assetRecord -> assetRecord.type != AssetRecord.Type.SOLD)
                .mapToDouble(ReturnUtil::getPrice)
                .sum();
        return (endValue - initialValue) / initialValue;
    }

    public static double getXirr(List<AssetRecord> totalAssetRecords) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        List<Transaction> transactions = totalAssetRecords.stream()
                .map(assetRecord -> new Transaction(getAssetRecordPriceForXirr(assetRecord),
                        dateFormat.format(assetRecord.date.getTime())))
                .collect(Collectors.toList());
        try {
            return new Xirr(transactions).xirr();
        } catch (IllegalArgumentException | OverflowException e) {
            return Double.NaN;
        }

    }

    public static double getAssetRecordPriceForXirr(AssetRecord assetRecord) {
        if (assetRecord.type == AssetRecord.Type.BOUGHT) {
            return -1 * assetRecord.amount * assetRecord.price;
        } else if (assetRecord.type == AssetRecord.Type.SOLD) {
            return assetRecord.amount * assetRecord.price;
        } else if (assetRecord.type == AssetRecord.Type.DIVIDEND) {
            return assetRecord.price;
        } else {
            throw new RuntimeException("Asset record type is not defined.");
        }
    }
}
