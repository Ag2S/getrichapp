package org.alan.getrich;

import java.util.function.Consumer;
import java.util.function.Supplier;

public abstract class Result<T> {
    public abstract Result<T> onSuccess(Consumer<T> dataConsumer);

    public abstract Result<T> onFailure(Consumer<Throwable> exceptionConsumer);

    static public <T> Result<T> success(T data) {
        return new Result<T>() {
            @Override
            public Result<T> onSuccess(Consumer<T> dataConsumer) {
                dataConsumer.accept(data);
                return this;
            }

            @Override
            public Result<T> onFailure(Consumer<Throwable> exceptionConsumer) {
                return this;
            }
        };
    }

    static public <T> Result<T> failure(Throwable exception) {
        return new Result<T>() {
            @Override
            public Result<T> onSuccess(Consumer<T> dataConsumer) {
                return this;
            }

            @Override
            public Result<T> onFailure(Consumer<Throwable> exceptionConsumer) {
                exceptionConsumer.accept(exception);
                return this;
            }
        };
    }

    static public <T> Result<T> runCatching(Supplier<T> supplier) {
        try {
            return success(supplier.get());
        } catch (Throwable exception) {
            return failure(exception);
        }
    }
}
